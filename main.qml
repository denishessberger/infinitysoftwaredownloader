import QtCore
import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Dialogs

import com.blackgrain.qml.quickdownload 1.0

ApplicationWindow {
    id: window
    width: 600
    height: 400
    visible: true
    title: qsTr("Infinity Chimp Update Download Utility")
    color: "#252525"

    readonly property string softwareListURL: "http://www.bretgeld.de/infinity/updates.txt"
    readonly property string libraryListURL: "http://www.bretgeld.de/infinity/chimp/library/list.txt"

    ListModel {
        id: softwareListModel
    }

    Download {
        id: download

        overwrite: true
        followRedirects: true

        onStarted: {
            statusPopUp.open();
        }

        onError: (errorString) => console.error("error!", errorString)

        onFinished: {
            statusPopUp.close();
        }

        function downloadTo(file) {
            download.destination = file
            download.start()
        }
    }

    function readTextFile(fileUrl){
        var xhr = new XMLHttpRequest;
        xhr.open("GET", fileUrl); // set Method and File
        xhr.onreadystatechange = function () {
            if(xhr.readyState === XMLHttpRequest.DONE){ // if request_status == DONE
                var response = xhr.responseText;

                populateModel(response);
            }
            console.log(xhr.responseText,xhr.statusText)
        }
        xhr.send(); // begin the request
    }

    function populateModel (textString) {
        var res = textString.split("\n");

        if (softwareListModel && softwareListModel.count !== 0)
            softwareListModel.clear();

        var i;
        for (i = 0; i < res.length - 1; i++) {
            softwareListModel.append({"url": res[i]})
        }

    }

    Component.onCompleted: readTextFile(softwareListURL)

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 8
spacing: 0

        RowLayout {
            Layout.fillWidth: true
            Layout.minimumHeight: 60
            Layout.maximumHeight: Layout.minimumHeight
            spacing: 10

            Button {
                id: softwareUpdateButton

                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.preferredWidth: 100

                checkable: true
                autoExclusive: true
                checked: true

                onClicked: readTextFile(softwareListURL)
                text: qsTr("Software Updates")
            }

            Button {
                id: libraryUpdateButton

                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.preferredWidth: 100

                checkable: true
                autoExclusive: true

                onClicked: readTextFile(libraryListURL)
                text: qsTr("Library Updates")

            }

        }

        ListView {
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.margins: 10

            id: listView
            clip: true

            model: softwareListModel

            highlightFollowsCurrentItem: true
            keyNavigationEnabled: true
            spacing: 4

            delegate: Rectangle {
                id: del

                width: listView.width
                height: 40

                required property string url
                required property int index

                color: listView.currentIndex === index ? "#909090" : "#454545"
                radius: 4
                ColumnLayout {
                    id: content
                    anchors.fill: parent
                    Text {
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                        text: del.url
                        font.pointSize: 12
                        horizontalAlignment: Qt.AlignHCenter
                        verticalAlignment: Qt.AlignVCenter
                        color: "white" }
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        console.log("Clicked " + index)
                        listView.currentIndex = index
                        download.url = "http://www.bretgeld.de/infinity/" + del.url
                    }
                }
            }
        }

        Button {
            id: downloadButton

            Layout.fillWidth: true
            Layout.minimumHeight: 60
            Layout.maximumHeight: Layout.minimumHeight

            onClicked: fileDialog.open()
            text: qsTr("Download Update")
            enabled: listView.currentItem
        }

        FileDialog {
            id: fileDialog
            currentFolder: StandardPaths.standardLocations(StandardPaths.DownloadLocation)[0]
            onAccepted: download.downloadTo(selectedFile)
            fileMode: FileDialog.SaveFile
            //selectedFile: listView.currentItem.url ?? ""
            nameFilters: ["Software Update Files (*.swu)"]

        }

        Popup {
            id: statusPopUp
            modal: true
            dim: true

            implicitWidth: window.width * 0.8
            implicitHeight: window.height * 0.5

            anchors.centerIn: Overlay.overlay


            ColumnLayout {
                anchors.fill: parent
                spacing: 20

                Text {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    font.pointSize: 12
                    text: qsTr("Downloading\n\n" + listView.currentItem.url ?? "")
                    horizontalAlignment: Qt.AlignHCenter
                    verticalAlignment: Qt.AlignVCenter
                }

                ProgressBar {
                    Layout.fillWidth: true
                    Layout.minimumHeight: 30
                    value: download.progress
                }
            }
        }
    }
}
